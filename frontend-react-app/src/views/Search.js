import React from 'react';
import RestBox from './RestBox.js'
import Config from '../config.js'
import CharityBox from './CharityBox.js'
import IncomeBox from './IncomeBox.js'

import STATIC from '../static_data.js'

class Search extends React.Component {
	constructor(props)
	{
		super(props);


		this.endpoint = Config.host + Config.endpoints[this.props.model];
		this.zipcodes = STATIC.zipcodes
		this.states = STATIC.states
		this.foodType = STATIC.foodType

		this.api_results = { } //caching old api result

		this.sortOptions =
		{
			"restaurants":
			{
				"Alphabetically": this.sortByAlpheticalOrder,
				"By Rating": this.sortByRating
			},
			"charities":
			{
				"Alphabetically": this.sortByAlpheticalOrder,
				//"By Income": this.sortByAmount
			},
			"income":
			{
				"Income": this.sortByAmount,
				"Zipcode": this.sortByZipcode
			}
		}
		
		this.queries =
		{
			"restaurants": 
			{
				"zipcode": null,
				"state": null,
				"city": null,
				"lower": null,
				"upper": null,
				"query": null,
				"foodType":null,
				"sorting":null
			},
			"charities":
			{
				"zipcode": null,
				"state": null,
				"city": null,
				"lower": null,
				"upper": null,
				"query": null,
				"foodType":null,
				"sorting":null
			} ,
			"income":
			{
				"zip": null,
				"slower": null,
				"supper": null
			}
		}
		this.state = {
			queries: this.queries[this.props.model],
			toRender: [],
			sortOption: Object.keys(this.sortOptions[this.props.model])[0],
			reversed: false,
			loading: false,
			page : 0
		}

		this.getFilterComponent = this.getFilterComponent.bind(this);

		this.getData = this.getData.bind(this)
		this.changePage = this.changePage.bind(this)
		this.getDataBuildAndFilter = this.getDataBuildAndFilter.bind(this);
		this.searchAction = this.searchAction.bind(this);
		this.relationAction = this.relationAction.bind(this);
		this.updateQueries = this.updateQueries.bind(this)
		this.handleSort = this.handleSort.bind(this)
		this.handleReverseSort = this.handleReverseSort.bind(this)
	}

	sortByAlpheticalOrder(a, b)
	{
		return a.name.toLowerCase().trim().localeCompare(b.name.toLowerCase().trim());
	}

	sortByRating(a,b)
	{
		return b.rating - a.rating;
	}


	sortByZipcode(a, b)
	{
		return a.ZIP - b.ZIP
	}

	sortByAmount(a,b)
	{
		return b.AMOUNT - a.AMOUNT;
	}




	componentDidMount()
	{

		this.getDataBuildAndFilter(
			(data) => 
			{
				this.setState({toRender : data})
			}
		);
	}


	getDataBuildAndFilter(callback, purge = false)
	{
		this.setState({loading: true})
		this.getData( (data) =>
			{
				this.setState({loading: false})

				try
				{
					let toRender = []
					data.sort(this.sortOptions[this.props.model][this.state.sortOption]);
					if(this.state.reversed)
						data.reverse()
					for(let i in data)
					{
						if(this.props.model == "restaurants")
						{
							toRender.push(<RestBox key={data[i].id} relation={this.relationAction} query={this.state.queries.query} {...data[i]}/>)
						} else if(this.props.model == "charities")
						{
							toRender.push(<CharityBox key={data[i].id} query={this.state.queries.query} relation={this.relationAction} {...data[i]}/>)
						} else if(this.props.model == "income")
						{
							toRender.push(<IncomeBox key={data[i].id} query={this.state.queries.query} relation={this.relationAction} {...data[i]}/>)
						}
					}
					callback(toRender)
				} catch (e)
				{
					callback(null);
				}
			}
		, purge);

	}

	handleSort(e)
	{
		this.state.sortOption = e.target.value;
		this.getDataBuildAndFilter(
			(data) => 
			{
				this.setState({toRender : data})
			}
		);

	}

	handleReverseSort(e)
	{

		this.state.reversed = e.target.checked
		this.getDataBuildAndFilter(
			(data) => 
			{
				this.setState({toRender : data})
			}
		);

	}

	searchAction()
	{
		this.getDataBuildAndFilter(
			(toRender) => 
			{
				this.setState({page: 0, toRender : toRender})
			}
		)

	}

	relationAction(id)
	{
		if(this.props.model == "restaurants")
		{
			this.props.handleClick("/restinfo/"+id)
		} else if(this.props.model == "charities"){
			this.props.handleClick("/charityinfo/"+id)
		} else if(this.props.model == "income"){
			this.props.handleClick("/zipinfo/"+id)
		}
	}

	getData(callback, purge = false)
	{	
		let get_url = this.endpoint + "?"
		for(let query in this.state.queries)
		{
			if(this.state.queries[query] != null && this.state.queries[query] != "")
			{
				get_url += query+"="+this.state.queries[query]+"&"
			}
		}
		if(get_url.charAt(get_url.length - 1) == '&')
			get_url = get_url.substr(0,get_url.length - 1)
		if(purge || !(get_url in this.api_results) || this.api_results[get_url] == null)
		{
			fetch(get_url, {
					method:'GET',
					credentials: 'include',
				    headers: {
					    'Accept': 'application/json',
					    'Content-Type': 'application/json'
					}
				}).then(response => response.json())
			    .then(json => {
			    	this.api_results[get_url] = json //cache it
			    	callback(json);
			    }).catch(e =>
			    {
			    	callback(null);
			        console.log("Failed to fetch")
			    })
		} else
		{
			callback(this.api_results[get_url])
		}
		
	}

	/*
	update queries
	*/
	updateQueries(key, category) 
	{
		if(key == "")
			key = null
		this.state.queries[category] = key
		this.setState(this.state)
	}

	changePage(event)
	{
		this.setState({page : parseInt(event.target.value)});
	}


	handleSorting(event)
	{
		this.setState({sorting : parseInt(event.target.value)});
	}

	getFilterComponent()
	{
		let states = []
		for(let key in this.states)

		{
			states.push(<a className='dropdown-item' onClick={this.updateQueries.bind(this, key, "state")} key={states.length}>{this.states[key]}</a>)
		}
		let foodType = []
		for(let key in this.foodType)
		{
			foodType.push(<a className='dropdown-item' onClick={this.updateQueries.bind(this, this.foodType[key], "foodType")} key={foodType.length}>{this.foodType[key]}</a>)
		}
		let category_filte= <div className="col-md-3 mb-6">
							           		<label htmlFor="state">Category</label>
								            <a className='nav-link dropdown-toggle' href='#' id='foodType' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>{this.state.queries.foodType == null ? "Choose..." : this.state.queries.foodType}</a>
								            <div className='dropdown-menu' style={{'height': '30vh', 'overflow': "scroll"}} aria-labelledby='foodType'>
								            	<a className='dropdown-item' onClick={this.updateQueries.bind(this, null, "foodType")} key={foodType.length}>Reset</a>
								             	{foodType}
								            </div>
						            	</div>
		if(this.props.model == "restaurants" || this.props.model == "charities")
		{
			return (<div className="mb-2">
					 	<h5>Find {this.props.model}</h5>
			            	<div className="mb-2">
			            		<label htmlFor="state">Query</label>
			            		<input type="search" id ="searchbar" className="form-control" placeholder="Search..." onChange={(e) => this.state.queries.query = e.target.value} required="" autoFocus=""></input>
				            </div>
				           	<div className="row">
				           	 	<div className="col-md-5 mb-6">
				           	 		<h6 className="mb-3">Area Filtering</h6>

						            <div className="row">
							            <div className="col-md-3 mb-6">
							            	<label htmlFor="city">City</label>
								            <input type="search" id ="city" className="form-control" placeholder="..." onChange={(e) => this.state.queries.city = e.target.value} required="" autoFocus=""></input>
							            </div>
						            	<div className="col-md-3 mb-6">
							           		<label htmlFor="state">State</label>
								            <a className='nav-link dropdown-toggle' href='#' id='state' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>{this.state.queries.state == null ? "Choose..." : this.state.queries.state}</a>
								            <div className='dropdown-menu' style={{'height': '30vh', 'overflow': "scroll"}} aria-labelledby='state'>
								            	<a className='dropdown-item' onClick={this.updateQueries.bind(this, null, "state")} key={states.length}>Reset</a>
								             	{states}
								            </div>
						            	</div> {this.props.model == "restaurants" ? category_filte : null}
							            <div className="col-md-3 mb-6">
							            	<label htmlFor="zipcode">Zipcode</label>
								            <input type="search" id ="zipcode" className="form-control" placeholder="..." onChange={(e) => this.state.queries.zipcode = e.target.value} required="" autoFocus=""></input>
							            </div>
					            	</div>
					            </div>
					        	<div className="col-md-5 mb-6">
					            	<h6 className="mb-3">List From</h6>
					            	<div className="row">
							            <div className="col-md-3 mb-6">
							            	<label htmlFor="lower">Lower Bound</label>
								            <input type="number" id ="lower" className="form-control" placeholder="$$$" onChange={(e) => this.state.queries.lower = (e.target.value != "" ? parseFloat(e.target.value)  * 100 : null)} required="" autoFocus=""></input>
							            </div>
						            	<div className="col-md-3 mb-6">
							           		<label htmlFor="upper">Upper Bound</label>
								            <input type="number" id ="upper" className="form-control" placeholder="$$$" onChange={(e) => this.state.queries.upper = (e.target.value != "" ? parseFloat(e.target.value)  * 100 : null)} required="" autoFocus=""></input>
						            	</div>
					            	</div>
					            </div>
			            	</div>

			            <hr className="mb-4"/>
			            <a className="btn btn-primary btn-large" onClick={this.searchAction.bind(this)} role="button">Search</a>
		            </div>)
		} else if(this.props.model == "income")
		{
			return (<div className="mb-2">
					 	<h5>Find {this.props.model}</h5>
			            	<div className="mb-2">
			            		<label htmlFor="state">Zipcode</label>
			            		<input type="search" id ="searchbar" className="form-control" placeholder="Search..." onChange={(e) => this.state.queries.zip = e.target.value} required="" autoFocus=""></input>
				            </div>
				           	<div className="row">
					        	<div className="col-md-5 mb-6">
					            	<h6 className="mb-3">Income Filtering</h6>
					            	<div className="row">
							            <div className="col-md-3 mb-6">
							            	<label htmlFor="lower">Lower Bound</label>
								            <input type="number" id ="lower" className="form-control" placeholder="$$$" onChange={(e) => this.state.queries.slower = (e.target.value != "" ? parseFloat(e.target.value)  * 100 : null)} required="" autoFocus=""></input>
							            </div>
						            	<div className="col-md-3 mb-6">
							           		<label htmlFor="upper">Upper Bound</label>
								            <input type="number" id ="upper" className="form-control" placeholder="$$$" onChange={(e) => this.state.queries.supper = (e.target.value != "" ? parseFloat(e.target.value)  * 100 : null)} required="" autoFocus=""></input>
						            	</div>
					            	</div>
					            </div>
			            	</div>

			            <hr className="mb-4"/>
			            <a className="btn btn-primary btn-large" onClick={this.searchAction.bind(this)} role="button">Search</a>
		            </div>)
		}


	}

	render()
	{
		let pagination = []
		if(this.state.toRender != null)
		{
			for(var i = 0; i < this.state.toRender.length; i+=12)
			{
				pagination.push(<option value={i/12}>{i/12+1}</option>);
			}
		}
		var options = []
		for(let key in this.sortOptions[this.props.model])
		{
			options.push(<option value={key} key={key}>{key}</option>)
		}
		let filterComponent = this.getFilterComponent()
		return (
			
			<div>
				 <div className='jumbotron'>
				 	{filterComponent}
		 			<div className="mb-2">
		 				<div className="row">
		 						<div className="col-sm-2">
					            	<label htmlFor="filterfrom"  >Sort : </label>
					        		<select value={this.state.sorting} onChange={(e) =>  this.handleSort(e)}>
										{options}
					        		</select>
					        	</div>
					        	<div className="col-sm-1">
				        			<input type="checkbox" onChange={(e) => this.handleReverseSort(e)}/> Reversed
				        		</div>

					    </div>
		 				<div style={{marginLeft:"35vw"}}>
			    			<img style={{display: this.state.loading ? "block" : "none", width: "30vw"}} src="http://css-workshop.com/wp-content/themes/mocca/img/loader.gif" alt="loading"/>
			    		</div>
			    		<div className="mb-2">
			    			<a style={{display: this.state.loading ? "none": "inline-block"}} className="btn btn-primary btn-sm" onClick={() => this.getDataBuildAndFilter((toRender) => this.setState({toRender: toRender}), true)} role="button">Reload</a>
				    	</div>
				    	<div className='row'>
							{this.state.toRender != null ? this.state.toRender.slice(this.state.page * 12, (this.state.page+1) * 12) : null}
						</div>
						<div className="paginate wrapper">
					        <select value={this.state.page} onChange={(this.changePage)} >
					            {pagination}
					        </select>
				    	</div>
				    </div>

				</div>
	    	</div>
	    )
	}
}

export default Search;
