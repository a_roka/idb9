import React from 'react';
import Config from '../config.js'
import AboutPerson from "./AboutPerson"
import '../css/index.css'

class AboutPage extends React.Component {
	constructor(props)
	{
		super(props);

		this.state = {
				members: Config.members,
				commits: 0,
				issues: 0,
				tests: 0,
				repo: Config.gitlab_repo,
				postman: Config.api_postman
		}

		this.tools = {
			"AWS" : {
				"description": "Manage domain, host website and database"
			},
			"React.JS":
			{
				"description": "Javascript library used to create UI components."
			},
			"React Router":
			{
				"description": "React extension used to manage application states."
			},
			"Jest":
			{
				"description": "Unit test runner used to run our React Component tests."
			},
			"Enzyme":
			{
				"description": "Unit testing utility used to wrap React Components and call measuring functions on them."
			},
			"Flask": {
				"description": "Microframework for building web applications and APIs."
			},
			"Flask-SQLAlchemy": {
				"description": "Object Relational Mapper used to construct our database."
			},
			"Flask-Marshmallow": {
				"description": "Serializer used to convert SQLAlchemy objects to JSON format."
			},
			"SQLIFY":
			{
				"description": "Convert JSON file to SQL queries"
			},
			"Postman":
			{
				"description": "Retrieve information of the APIs we use and also test and design our own database"
			},
			"PlantUML":
			{
				"description": "We use it to plan out the structure of our database models"
			},
			"D3js":
			{
				"description": "Visualizations of database models"
			},
			"Docker":
			{
				"description": "Container management platform used to create a portable sandbox for development."
			}
		}
	}

	componentDidMount()
	{
		//parse # commits
		fetch(Config.host + Config.endpoints.gitlab,
	    {
	      method: 'GET',
	      credentials: 'include',
	      crossDomain : true,
	      headers: {
	          'Accept': 'application/json',
	          'Content-Type': 'application/json',
	          'Access-Control-Allow-Origin':'*'
	      }
	    }).then(response => response.json())
	    .then(json => {
	          for(var member in json.members)
	          {
	          	if(member in this.state.members)
	          	{
		          	this.state.members[member].issues = json.members[member].issues
		          	this.state.members[member].commits = json.members[member].commits
		          	this.state.members[member].tests = json.members[member].tests
		        }
	          }
	          this.state.issues = json.total_issues
	          this.state.commits = json.total_commits
	          this.state.tests = json.total_tests
	          this.setState(this.state)
	    }).catch(e =>
	    {
	        console.log("Failed to fetch")
	    })
	}

	render()
	{
		this.abouts = []

		let tools_used = []

		for(let tool in this.tools)
		{
			tools_used.push(<div key={tools_used.length} className="col-md-3 d-flex align-items-stretch">
								<div className="card mb-3 box-shadow">
									<div className="card-body">
										<h5 className="card-title">{tool}</h5>
										<p className="card-text">{this.tools[tool].description}</p>
									</div>
								</div>
							</div>)
		}

		for(var person in this.state.members)
		{
			this.abouts.push(<div className='col-md-4' key={person}><AboutPerson name={person} 
						key={person} {...this.state.members[person]}/></div>)
		}
		return (
			 <div class='jumbotron'>
			 	 <div className="container">
			 	 	<div className="row" style={{display: "flex", justifyContent: "space-between"}}>
							<div className="jumbotron col-md-5 bg-light">
								<h1> About The Site</h1>
								<p>
							 	Redistribution of resources is a civic difficulty that plagues almost any aspect of charity, be it receiving, gifting, or distributing. CharityLink is a platform designed to give restaurant and food distributors information on local charities, and give charities an effective way to find the most in-need locations to distribute the donated food. Also, this gives individual users the ability to find charitable small businesses or volunteer opportunities near them.
								</p>
							</div>

							<div className="jumbotron col-md-5 bg-light">
								<h1>About Our Data</h1>
								<p>
							 	Almost all of the attributes listed under each model are included in
								their instance pages, as well as previewed inside their grid box on model
								pages. Many restaurants are linked to many charities, with the unifying
								property being the zip code each instance is located in. One zip code can and
								will have many instances of restaurants and charities within it. Average
								income levels of geographical areas were tabulated by zip codes in the API, to
								which we then also attributed the corresponding city and state to each. Then
								the other two models, charities and restaurants, also inherit those attributes
								based on their locale’s zip code.
								</p>
							</div>
					</div>
				 </div>
				 <div className='container'>
				     <h1 className="text-center">About the CharityLink Team</h1>
				     <div className='row'>
			     		<div className="col-12 col-sm-3">
			     			<ul className="list-group">
			     				<li className="list-group-item">Total Commits: {this.state.commits}</li>
			     				<li className="list-group-item">Total Issues: {this.state.issues}</li>
			     				<li className="list-group-item">Total Tests: {this.state.tests}</li>
			     			</ul>
			     		</div>
			     	</div>
					 <div className='row justify-content-center'>
					 	{this.abouts}
					 </div>
			     </div>
				 <div className="album py-4">

				 	<h1 className="text-center">Tools Used</h1>
				 	<div className='container'>
				 		<div className="row">
				 			{tools_used}
				 		</div>
				 	</div>
				 </div>
			 </div>
	    )
	}
}
export default AboutPage