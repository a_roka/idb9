import React from "react";
import Config from "../../config.js";
import { BarChart } from "react-easy-chart";
import static_data from "../../static_data.js";
import "../../css/d3.css";
import * as d3 from "d3";

function dataMaxFun(max, p) {
    if (max["y"] != null) return max["y"];
    return p["y"] > max ? p["y"] : max;
}

function createBarChart(id, data) {
    // set the dimensions and margins of the graph
    var margin = {
            top: 20,
            right: 20,
            bottom: 50,
            left: 70
        },
    width = 0,
    height = 0
    try { 
        width = d3.select(id)
            .node()
            .getBoundingClientRect().width -
            margin.left -
            margin.right
    } catch {
        width = 0
    }

    try { 
        height = d3.select(id).node().getBoundingClientRect().height - margin.top - margin.bottom;
    } catch {
        height = 0
    }

    // set the ranges
    var x = d3
        .scaleBand()
        .range([0, width])
        .padding(0.1);
    var y = d3.scaleLinear().range([height, 0]);

    // append the svg object to the body of the page
    // append a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    var svg = d3
        .select(id)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Scale the range of the data in the domains
    x.domain(
        data.map(function(d) {
            return d["x"];
        })
    );
    y.domain([
        0,
        d3.max(data, function(d) {
            return d["y"];
        })
    ]);

    // append the rectangles for the bar chart
    svg
        .selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", function(d) {
            return x(d["x"]);
        })
        .attr("width", x.bandwidth())
        .attr("y", function(d) {
            return y(d["y"]);
        })
        .attr("height", function(d) {
            return height - y(d["y"]);
        });

    // add the x Axis
    svg
        .append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    // add the y Axis
    svg.append("g").call(d3.axisLeft(y));

    svg
        .append("text")
        .attr(
            "transform",
            "translate(" + width / 2 + " ," + (height + margin.top + 20) + ")"
        )
        .style("text-anchor", "middle")
        .text("Poverty Level");

    svg
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - margin.left)
        .attr("x", 0 - height / 2)
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text(
            id
            .charAt(9)
            .toUpperCase()
            .concat(id.slice(10).concat(" Per"))
        );
}

class D3BarGraph extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    this.setState({
      loading: true
    });
    var sampleData = {};
    if (this.props.type == "chvolunteer") {
      sampleData = static_data.volunteer; /* Sample random data. */
    }
    if (this.props.type == "chfoodpantry") {
      sampleData = static_data.foodpantry; /* Sample random data. */
    }
    createBarChart("#statesvg".concat(this.props.type.substr(2)), sampleData);

    //var thus = d3.select("statesvg".concat(this.props.type))
    this.setState({
      loading: false
    });
  }
  render() {
    return (
      <div
        className="jumbotron"
        id={"bargraphd3".concat(this.props.type.substr(2))}
      >
        <h3>
          {this.props.type.charAt(2).toUpperCase() + this.props.type.slice(3)}
          Stats Map
        </h3>
        <img
          style={{
            display: this.state.loading ? "block" : "none",
            width: "30vw"
          }}
          src="http://css-workshop.com/wp-content/themes/mocca/img/loader.gif"
          alt="loading"
        />
        <div className="container">
          <svg
            ref={node => (this.node = node)}
            preserveAspectRatio="xMinYMin meet"
            viewBox="0 0 1280 800"
            id={"statesvg".concat(this.props.type.substr(2))}
          >
          </svg>
        </div>
      </div>
    );
  }
}

export default D3BarGraph;

// <div ref="graph">
//   <BarChart
//       axisLabels={{x: "Poverty Level", y: "Avg".concat(this.props.type)}}
//       axes
//       data={this.props.type == 'chvolunteer' ? static_data.volunteer : static_data.foodpantry}
//     />
// </div>
