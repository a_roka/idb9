import React from 'react';
import jumbotron from '../css/jumbotron.css'
import utils from '../utils/utils.js'
import SimpleMap from './ui/map.js'

/* Restaurant box */
class IBox extends React.Component{
	constructor(props){
		super(props);
		
		this.state = {
			modalIsOpen : false
		}	
	}

	

	render(){
		return (
			<div style={{marginTop: "20px"}} className='col-md-3 col-md-offset-3 hover-box'>
				<div className='card' style={{minHeight:"100%"}}>
					<div onClick={() => this.props.relation(this.props.ZIP)}>
					<div className='card-img-top'>
				    	<SimpleMap height="20vh" latitude={this.props.latitude} longitude={this.props.longitude} key={this.props.id}/>
				    </div>
				      	<div className="card-body">
				      		<h4>{utils.pad(this.props.ZIP, 5)}</h4>
						    <ul>
						    	 <li>Average income: ${this.props.AMOUNT / 100} / Month</li>
			      			</ul>
				      		
		      			</div>
		      		</div>
      			</div>
	    	</div>
      );
	}
}

export default IBox;
