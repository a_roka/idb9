import React from 'react';
import AboutPage from './AboutPage';
import expect from 'expect';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests AboutPage.js", () => {
	global.fetch = fetch
    test("Renders without exploding", () => {

      const wrapper = shallow(<AboutPage />);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      
    });
  
  });
