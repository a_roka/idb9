import expect from 'expect';
import CharityInstance from './CharityInstance.js';
import React from 'react';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("CharityInstance.js", () => {
  test("Renders without exploding", () => {
  	const wrapper = shallow(<CharityInstance />);
    expect(wrapper.length).toBe(1);
  });

});