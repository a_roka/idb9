import expect from 'expect';
import RestInstance from './RestInstance.js';
import React from 'react';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Rest Instance.js", () => {
  test("Renders without exploding", () => {
  	const wrapper = shallow(<RestInstance />);
    expect(wrapper.length).toBe(1);
  });

});