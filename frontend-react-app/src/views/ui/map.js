import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import MyGreatPlace from './marker.js';
const AnyReactComponent = ({ text }) => <div>{text}</div>;
 
class SimpleMap extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      latitide : this.props.latitide,
      longitude : this.props.longitude
    }
  }


  static getDerivedStateFromProps(props, current_state) {
    if (current_state.latitude !== props.latitude) {
      return {
        latitude: props.latitude,
        longitude: props.longitude
      }
    }
    return null
  }



  render() {
    let center = {
        lat: this.state.latitude,
        lng: this.state.longitude
    }
    return (
      // Important! Always set the container height explicitly
      <div style={{height: this.props.height, width: '100%'}}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyBGn4yiYN28XAAEPsFsvpCKNLyJgxfFllI'}}
          center={center}
          defaultZoom={11}
        >
         <MyGreatPlace lat={center.lat}
            lng={center.lng}
            text={this.props.name}
            text={''} /* road circle */   />
        </GoogleMapReact>
      </div>
    );
  }
}
 
export default SimpleMap;