import React from 'react';
import SimpleMap from './map';
import expect from 'expect';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests SimpleMap.js", () => {

    test("Renders without exploding", () => {

      const wrapper = shallow(<SimpleMap />);

      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();

    });

  });
