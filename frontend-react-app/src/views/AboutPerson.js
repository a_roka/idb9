import React from 'react';
import Config from '../config.js'

/* About Person box */
class AboutPerson extends React.Component {
	constructor(props)
	{
		super(props);
	}

	componentDidMount()
	{
	    //parse # issues
	 //    if(typeof(this.state.name) != 'undefined')
	 //    {
		// 	fetch(Config.host + Config.endpoints.gitlab_tests+"/"+this.state.name.split(" ")[0],
		//     {
		//       method:'GET',
		//       credentials: 'include',
		//       crossDomain : true,
		//       headers: {
		//           'Accept': 'application/json',
		//           'Content-Type': 'application/json',
		//           'Access-Control-Allow-Origin':'*'
		//       }
		//     }).then(response => response.json())
		//     .then(json => {
		//           this.setState({tests: json["count"]})
		//     }).catch(e =>
		//     {
		//         console.log("Failed to fetch")
		//     })
		// }
	}

	render()
	{
		return (
			<div className='card mb-3 box-shadow'>
				<div className='card-body'>
					<div>
						<img className='card-img-top' style={{width: '100%', height: '100vw', objectFit: 'cover', maxHeight: '325px'}} src={this.props.picture}/>
						<h5 className="card-title">{this.props.name}</h5>
					</div>
					<p className="card-text">{this.props.bio}</p>
				</div>
				<ul className="list-group list-group-flush">
				 	<li className="list-group-item"> Commits: {this.props.commits} </li>
				 	<li className="list-group-item"> Issues: {this.props.issues} </li>
				 	<li className="list-group-item"> Tests: {this.props.tests} </li>
				 	<li className="list-group-item"> Responsibities: {this.props.position} </li>
                </ul>

			</div>
	    )
	}
}

export default AboutPerson;

